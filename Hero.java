
public class Hero {
	private int posx;
	private int posy;
	
	public Hero(int x, int y) {
		setPosx(x);
		setPosy(y);
	}

	public int getPosx() {
		return posx;
	}

	public void setPosx(int posx) {
		this.posx = posx;
	}

	public int getPosy() {
		return posy;
	}

	public void setPosy(int posy) {
		this.posy = posy;
	}
}
