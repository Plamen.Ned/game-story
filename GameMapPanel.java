import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GameMapPanel extends Panel {
	Karta karta;
	
	int sila = 0;
	int kruv = 0;
	GameMapPanel(Karta k) {
		
		
		karta = k;
		GameKeyListener gkl = new GameKeyListener(this);
		GameMouseListener gml = new GameMouseListener(this);
		setLayout(null);
		addKeyListener(gkl);
		addMouseListener(gml);
		
	}
	
	public void paint(Graphics g) {
//		HERO
		File f = new File("Hero.png");
		try {
			BufferedImage bi = ImageIO.read(f);
			g.drawImage(bi, karta.hero.getPosx() * 60, karta.hero.getPosy() * 60, 60, 60, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
//		GUNTER
//		File p = new File("gunter.png");
//		try {
//			BufferedImage bi = ImageIO.read(p);
//			g.drawImage(bi, karta.gunter.getPosX() * 60, karta.gunter.getPosY() * 60, 60, 60, null);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		
//		Gira
		File m = new File("gira.png");
		try {
			BufferedImage bi = ImageIO.read(m);
			g.drawImage(bi, karta.gira.getPlacex1() * 60, karta.gira.getPlacey1() * 60, 60, 60, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
		  if(karta.hero.getPosx()==karta.gira.getPlacex1() && karta.hero.getPosy()==karta.gira.getPlacey1() || karta.hero.getPosx()==karta.gira.getPlacex1() && karta.hero.getPosy()==karta.gira.getPlacey1()) {
//			  karta.spawncoin(this);
			  karta.gira.setPlacex1((int) Math. floor(Math. random()*(karta.sizex)));
			  karta.gira.setPlacey1((int) Math. floor(Math. random()*(karta.sizey)));
			  sila++;
	          repaint();
	         
	      }
		  // HEALTH
		  File h = new File("heart.png");
			try {
				BufferedImage bi = ImageIO.read(h);
				g.drawImage(bi, karta.health.getPlaceX1() * 60, karta.health.getPlaceY1() * 60, 60, 60, null);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			 if(karta.hero.getPosx()==karta.health.getPlaceX1() && karta.hero.getPosy()==karta.health.getPlaceY1() || karta.hero.getPosx()==karta.health.getPlaceX1() && karta.hero.getPosy()==karta.health.getPlaceY1()) {
//				  karta.spawncoin(this);
				  karta.health.setPlaceX1((int) Math. floor(Math. random()*(karta.sizex)));
				  karta.health.setPlaceY1((int) Math. floor(Math. random()*(karta.sizey)));
				  kruv++;
		          repaint();
		         
		      }
		
		
		int sizexInPixels = karta.sizex * 60;
		int sizeyInPixels = karta.sizey * 60;
		
		for (int i = 0; i <= karta.sizex; i++) {
			g.drawLine(i * 60, 0, i * 60, sizeyInPixels);
		}
		for (int i = 0; i <= karta.sizey; i++) {
			g.drawLine(0, i * 60, sizexInPixels, i * 60);
		}
	}
}
