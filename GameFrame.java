import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.swing.Timer;

import javax.imageio.ImageIO;

public class GameFrame extends Frame {
	GameMapPanel mapPanel;
	Label label;
	Label label1;
	Label label2;
	Label label3;
	Label label4;
	Label label5;
	Label label6;
	Label label7;
	Label label8;
	Label label9;
	Label label10;
	Label label11;
	Button button;
	TextField textField;
	TextArea textArea;

	GameFrame(Karta k) {
		GameWindowListener gwl = new GameWindowListener(this);
		addWindowListener(gwl);
		
		mapPanel = new GameMapPanel(k);
		mapPanel.setBounds(30, 30, 620, 620);
		this.add(mapPanel);
		
		button = new Button();
		button.setBounds(k.sizex*90, 30, 100, 30);
		button.setLabel("����������");
		this.add(button);
		
		GameButtonActionListener al = new GameButtonActionListener(this);
		button.addActionListener(al);
		
		label = new Label();
		label.setBounds(k.sizex*90, 90, 600, 30);
		label.setText("");
		this.add(label);
		
		// istoriq
		label2 = new Label();
		label2.setBounds(k.sizex*90, 130, 600, 30);
		label2.setText("���� � ������� ��: ������ �� ��.");
		this.add(label2);
		
		label3 = new Label();
		label3.setBounds(k.sizex*90, 150, 600, 30);
		label3.setText("��: ��");
		label.setVisible(true);
		this.add(label3);
		
		label4 = new Label();
		label4.setBounds(k.sizex*90, 170, 600, 30);
		label4.setText("������: �������� �� �� ����� ����� � ���������� �� ����. ���� �� ����� �� �� ������� ���.");
		this.add(label4);
		
		label5 = new Label();
		label5.setBounds(k.sizex*90, 190, 600, 30);
		label5.setText("��: �� ��� �� ��?");
		label5.setVisible(true);
		this.add(label5);
	
		label6 = new Label();
		label6.setBounds(k.sizex*90, 210, 600, 30);
		label6.setText("������: �� ��� ����� �� ����� �� ���� ����� �� ������ �� �������.");
		label6.setVisible(true);
		this.add(label6);
		
		label7 = new Label();
		label7.setBounds(k.sizex*90, 230, 600, 30);
		label7.setText("������: ����� �� � ������.");
		label7.setVisible(true);
		this.add(label7);
	
		label8 = new Label();
		label8.setBounds(k.sizex*90, 250, 600, 30);
		label8.setText("��: �����, �� ���.");
		label8.setVisible(true);
		this.add(label8);
		
		label9 = new Label();
		label9.setBounds(k.sizex*90, 270, 600, 30);
		label9.setText("������: ����� � ������ ������ ����. ��� ������� �� �� ����� ��� �� ������. ");
		label9.setVisible(true);
		this.add(label9);
		
		label10 = new Label();
		label10.setBounds(k.sizex*90, 290, 600, 30);
		label10.setText("������: ������ �� ������ �������� � ������ ����������.");
		label10.setVisible(true);
		this.add(label10);
		
//����
		label1 = new Label();
		label1.setBounds(k.sizex*90, 310, 600, 30);
		label1.setText("����: " + mapPanel.sila);
		label1.setVisible(true);
		this.add(label1);
		
		// kruv
		label11 = new Label();
		label11.setBounds(k.sizex*90, 330, 600, 30);
		label11.setText("����: " + mapPanel.kruv);
		label11.setVisible(true);
		this.add(label11);
		
//		timer
		Timer time = new Timer(50,new ActionListener() {
		    

			@Override
			public void actionPerformed(ActionEvent e) {
			
				label1.setText("����: " + mapPanel.sila);
				label11.setText("����: " + mapPanel.kruv);
			}
		});
		time.start();
//		textField = new TextField();
//		textField.setBounds(k.sizex*90, 150, 600, 30);
//		textField.setText("������ ����� �� ����� ���");
//		this.add(textField);
//		
//		textArea = new TextArea();
//		textArea.setBounds(k.sizex*60, 210, 600, 90);
//		textArea.setText("������ �������� �� ����� ���");
//		this.add(textArea);
	}
	

}
