
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameKeyListener implements KeyListener {
	GameMapPanel panel;
	
	GameKeyListener(GameMapPanel p) {
		panel = p;
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		char c = e.getKeyChar();
		switch(c) {
			case 'w': panel.karta.hero.setPosy(panel.karta.hero.getPosy() - 1); break;
			case 's': panel.karta.hero.setPosy(panel.karta.hero.getPosy() + 1); break;
			case 'a': panel.karta.hero.setPosx(panel.karta.hero.getPosx() - 1); break;
			case 'd': panel.karta.hero.setPosx(panel.karta.hero.getPosx() + 1); break;
		}
		panel.repaint();
	}


	@Override
	public void keyReleased(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}
